<?php
namespace Appkita\PHPAuth;

class METHOD {
    public CONST KEY = 'key';
    public CONST TOKEN = 'token';
    public CONST DIGEST = 'digest';
    public CONST BASIC = 'basic';
}